import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import './App.css';

import LandingPage from './components/landing-page/LandingPage.js';
import Artist from './components/artist/Artist.js';
import ArtistTopTenTracks from './components/artist/ArtistTopTenTracks.js';

const API_ADDRESS = "https://spotify-api-wrapper.appspot.com";

class App extends React.Component {

    constructor(props) {
        super(props);

        this.updateArtistQuery = this.updateArtistQuery.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.searchForArtist = this.searchForArtist.bind(this);
        this.displayArtistTopTenTracks = this.displayArtistTopTenTracks.bind(this);
        this.displayArtist = this.displayArtist.bind(this);
    }

    state = {
        artistQuery: "",
        fetchedArtist: null,
        fetchedArtistTracks: [],
        fetchLoading: false,

        displayComponents: {
            landingPage: true,
            artistPage: false,
            artistTopTenPage: false,
        },
    };

    updateArtistQuery = (event) => {
        this.setState({
            artistQuery: event.target.value,
        })
    };

    handleKeyPress = (event) => {
        if (event.key === "Enter") {
            this.searchForArtist();
        }
    };


    // Check if the "this.state.artistQuery"
    searchForArtist = () => {

        // First clear the "fetchedArtist" in the state
        this.setState({
            fetchedArtist: null,
        });

        return (this.state.artistQuery !== "") ? (
            fetch(`${API_ADDRESS}/artist/${this.state.artistQuery}`)
                .then( (response) => response.json() )
                .then( (fetchedJSON) => {
                    // Check if the Artist total are more than one.
                    if (fetchedJSON.artists.total > 0) {
                        const artist = fetchedJSON.artists.items[0];
                        this.setState({fetchedArtist: artist});

                        fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                            .then( (response) => response.json() )
                            .then( (fetchedTracks) => {
                                this.setState({
                                    fetchedArtistTracks: fetchedTracks.tracks,
                                })
                            } )
                            .catch( (errorCallback) => { alert(errorCallback.message) } );
                    }
                } )
                .catch((errorCallback) => { alert(errorCallback.message) } ),
            // Call the displayArtist() function to change the displayed component
            this.displayArtist()
        ) : (
            alert("Search box can't be empty.")
        );
    };

    /*queryAPI = () => {

        // First clear the "fetchedArtist" in the state
        this.setState({
            fetchedArtist: null,
        });

        // Then query the API and fetch the Artist details
        fetch(`${API_ADDRESS}/artist/${this.state.artistQuery}`)
            .then( (response) => response.json() )
            .then( (fetchedJSON) => {
                // Check if the Artist total are more than one.
                if (fetchedJSON.artists.total > 0) {
                    const artist = fetchedJSON.artists.items[0];
                    this.setState({fetchedArtist: artist});

                    fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                        .then( (response) => response.json() )
                        .then( (fetchedTracks) => {
                            this.setState({
                                fetchedArtistTracks: fetchedTracks.tracks,
                            })
                        } )
                        .catch( (errorCallback) => { alert(errorCallback.message) } );
                }
            } )
            .catch((errorCallback) => { alert(errorCallback.message) } );

        // Call the displayArtist() function to change the displayed component
        this.displayArtist();
    };
*/

    displayArtist = () => {
        this.setState({
            displayComponents: {
                landingPage: false,
                artistPage: true,
                artistTopTenPage: false,
            },
        });
    };

    displayArtistTopTenTracks = () => {
        this.setState({
            displayComponents: {
                landingPage: false,
                artistPage: false,
                artistTopTenPage: true,
            }
        })
    };

    render() {
        return (
            <Router>
                <div className="App container-fluid" style= {{height: `${window.innerHeight}px`}}>

                    {/*<Route exact path= "/" component= { LandingPage }/>*/}
                    {
                        (this.state.displayComponents.landingPage) ? (
                            <LandingPage
                                updateArtistQuery = { this.updateArtistQuery }
                                handleKeyPress = { this.handleKeyPress }
                                searchForArtist = { this.searchForArtist }
                            />
                        ) : null
                    }

                    {/*<Route path= "/artist" component= { Artist }/>*/}
                    {
                        (this.state.displayComponents.artistPage) ? (
                            <Artist
                                updateArtistQuery = { this.updateArtistQuery }
                                handleKeyPress = { this.handleKeyPress }
                                searchForArtist = { this.searchForArtist }
                                fetchedArtist = { this.state.fetchedArtist }
                                displayArtistTopTenTracks = { this.displayArtistTopTenTracks }
                            />
                        ) : null
                    }

                    {/*<Route path= "/artist-top-ten-tracks" fetchedArtistTracks= { this.state.fetchedArtistTracks } component= { ArtistTopTenTracks }/>*/}
                    {
                        (this.state.displayComponents.artistTopTenPage) ? (
                            <ArtistTopTenTracks
                                updateArtistQuery= { this.updateArtistQuery }
                                handleKeyPress= { this.handleKeyPress }
                                searchForArtist= { this.searchForArtist }
                                fetchedArtistTracks= { this.state.fetchedArtistTracks }
                            />
                        ) : null
                    }
                </div>
            </Router>
        );
    }
}

export default App;
