import React, {Component} from 'react';
import { MDBBtn, MDBIcon } from "mdbreact";

class LandingPage extends Component {

    render() {

        const { updateArtistQuery, handleKeyPress, searchForArtist } =  this.props;

        return (
            <div className= "container-fluid" style= {{ height: `${window.innerHeight}px`, paddingTop: "100px" }}>
                <div className= "">
                    {/*<img src= { Logo } width= "200" alt="Muzik Logo" className= "img-fluid"/>*/}
                    <div className= "mx-auto logoImage"> </div>
                    <h1 className= "font-berkshire-swash font-weight-bold mt-5"  style= {{ fontSize: "4.3em", color: "darkRed" }}>M00zik K0m6</h1>
                </div>

                <div className= "container input-form">
                    <div className= "row">
                        <div className="col-8 mx-auto">
                            <div className="md-form input-group mb-3">
                                <input type="text" className="form-control"
                                       onChange= { updateArtistQuery }
                                       onKeyPress= { handleKeyPress }
                                       placeholder="Search for Artist"
                                       aria-label="Search for Artist"
                                       aria-describedby="MaterialButton-addon2" />
                                <div className="input-group-append">
                                    <MDBBtn onClick= { () => { searchForArtist() } } outline color="primary"  size="sm" type="submit" className="font-weight-bold" style= {{ padding: "13px 20px", borderRadius: 100 }}>
                                        <MDBIcon icon="search" className="mr-1" />
                                        Search
                                    </MDBBtn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LandingPage;